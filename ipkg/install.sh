#!/bin/sh
set -e

PWD=`pwd`
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
NAME=ipkg-utils
VERSION=1.7
RELEASE=$NAME-$VERSION
PACKAGE=$RELEASE.tar.gz
URL=ftp://ftp.osuosl.org/.2/nslu2/sources/$PACKAGE

execute() { echo "--> $@"; "$@"; }

echo
echo "-------------------------------------------------"
echo setup $NAME
echo "-------------------------------------------------"

execute cd $DIR

if [ -z ${PREFIX+x} ]
then
	PREFIX=$HOME/.local
	execute mkdir -p $PREFIX
fi

if [ ! -e $PACKAGE ]
then
	execute wget $URL
fi

if [ -d $RELEASE ]
then
	execute rm -rf $RELEASE
fi
execute tar -zxvf $PACKAGE

execute cd $RELEASE
execute patch -p1 < ../$RELEASE-user.patch
execute patch -p1 < ../$RELEASE-fix.patch
make install PREFIX=$PREFIX

execute cd $PWD
echo "-------------------------------------------------"

