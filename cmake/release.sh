#!/bin/sh
set -e

PROJECT=hedgehog
VERSION=1.0.0
MAINTAINER=john.doe@example.org

TOP="$PWD"
DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
BLD=`mktemp -d`
OUT=`mktemp -d`
TARGETS=`mktemp`
GENOPTS="-p $OUT -E $TARGETS -B $BLD"

pkgname=$PROJECT-`uname`-`uname -m`
fname=$(basename "${BASH_SOURCE[0]}")
name="${fname%.*}"

USAGE="
NAME
	$name - release project deliverables

SYNOPSIS
	./$fname [OPTION]

DESCRIPTION
	Build and releases project deliverables
	-d  release with debug symbols
	-m  email of the maintainer of the packages
"

while getopts :dm: opt; do
	case $opt in
		d)
			GENOPTS="$GENOPTS -d"
			pkgname="$pkgname-debug"
		;;

		m)
			MAINTAINER=$OPTARG
		;;

		\?)
			echo "$USAGE"
		;;
	esac
done

./gen.sh $GENOPTS -m $MAINTAINER

for target in `cat $TARGETS`
do
	cd $target
	make install
done

cd $OUT
for location in `find -name "*.ipk" -printf "%h\n" | uniq`
do
	cd $location
	ipkg-make-index -v . > Packages
	cd $OUT
done

pkgname="${pkgname,,}-${VERSION~}.tar.gz"
tar --exclude-vcs -pczf $DIR/$pkgname -C $OUT .

rm -rf $BLD $OUT $TARGETS
cd $TOP


