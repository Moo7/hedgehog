cmake_minimum_required(VERSION 2.8.12)

project(Hedgehog)

# find and add components
file(GLOB_RECURSE files CMakeLists.txt)
string(REPLACE "${CMAKE_SOURCE_DIR}/" "" components ${files})
string(REPLACE "CMakeLists.txt" " " components ${components})
string(REPLACE "/ " ";" components ${components})
foreach(component IN LISTS components)
	if(NOT ${component} STREQUAL " ")
		message("-- Added component '${component}'")
		add_subdirectory(${component})
	endif()
endforeach()

