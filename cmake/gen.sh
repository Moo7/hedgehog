#!/bin/sh
set -e

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
PREFIX=$DIR/out
MAINTAINER=john.doe@example.org

SRC=$DIR/examples
BLD=$DIR/build
BLD_TYPE=Release
GEN_TYPE="Eclipse CDT4 - Unix Makefiles"

IPKG_BINDIR=/usr/bin/
IPKG_LIBDIR=/usr/lib/

declare -a targets=(
"host,,`uname -m`,"
"mips,mips-openwrt-linux,ar71xx,opt/mips"
)

fname=$(basename "${BASH_SOURCE[0]}")
name="${fname%.*}"

USAGE="
NAME
	$name - generate cmake targets

SYNOPSIS
	./$fname [OPTION]

DESCRIPTION
	Generates cmake target makefiles and projects
	-p  install prefix [default: $PREFIX] 
	-d  select debug build instead of release
	-g  cmake generator [default: '$GEN_TYPE']
	-m  email of the maintainer of the packages
	-b  ipkg binary prefix [default: $IPKG_BINDIR]
	-l  ipkg library prefix [default: $IPKG_LIBDIR]
	-E  export make target locations to file
	-B  optional temporary build location
"

while getopts :p:dg:m:b:l:E:B: opt; do
	case $opt in
		p)
			PREFIX=$OPTARG
		;;

		d)
			BLD_TYPE=Debug
		;;

		g)
			GEN_TYPE=$OPTARG
		;;

		m)
			MAINTAINER=$OPTARG
		;;

		b)
			IPKG_BINDIR=$OPTARG
		;;

		l)
			IPKG_LIBDIR=$OPTARG
		;;

		E)
			GEN_OUT=$OPTARG
		;;

		B)
			BLD=$OPTARG
		;;

		\?)
			echo "$USAGE"
		;;
	esac
done

# remove root slash, add tail slash 
IPKG_BINDIR=${IPKG_BINDIR#/}
IPKG_BINDIR=${IPKG_BINDIR%/}/
IPKG_LIBDIR=${IPKG_LIBDIR#/}
IPKG_LIBDIR=${IPKG_LIBDIR%/}/

if [ ! -z ${GEN_OUT+x} ]
then
	if [ ! -e $GEN_OUT ]
	then
		echo "ERROR: output file '${GEN_OUT}' does not exist"
		exit 1
	fi
	GEN_OUT=`realpath $GEN_OUT`
fi

for target in "${targets[@]}"
do
	bld=`cut -d ',' -f 1 <<< $target`
	gcc=`cut -d ',' -f 2 <<< $target`
	arch=`cut -d ',' -f 3 <<< $target`
	prefix=`cut -d ',' -f 4 <<< $target`

	if [ -d $BLD/$bld ]
	then
		rm -rf $BLD/$bld
	fi
	mkdir -p $BLD/$bld
	cd $BLD/$bld

	if [ ! -z ${GEN_OUT+x} ]
	then 
		echo "$BLD/$bld" >> $GEN_OUT
	fi

	if [ "$prefix" == "" ]
	then
		CMAKE_OPTS="-DCMAKE_INSTALL_PREFIX=$PREFIX"
	else
		CMAKE_OPTS="-DCMAKE_INSTALL_PREFIX=$PREFIX/$prefix"
	fi

	CMAKE_OPTS="$CMAKE_OPTS -DCMAKE_BUILD_TYPE=$BLD_TYPE"
	CMAKE_OPTS="$CMAKE_OPTS -DCMAKE_SYSTEM_PROCESSOR=$arch"

	if [ ! "$gcc" == "" ]
	then
		CMAKE_OPTS="$CMAKE_OPTS -DCMAKE_SYSTEM_NAME=$gcc"
		CMAKE_OPTS="$CMAKE_OPTS -DCMAKE_C_COMPILER=$gcc-gcc"
		CMAKE_OPTS="$CMAKE_OPTS -DCMAKE_CXX_COMPILER=$gcc-g++"
	fi

	if [ "$arch" == "ar71xx" ]
	then
		CMAKE_OPTS="$CMAKE_OPTS -DIPKG_BINDIR=$IPKG_BINDIR"
		CMAKE_OPTS="$CMAKE_OPTS -DIPKG_LIBDIR=$IPKG_LIBDIR"
		CMAKE_OPTS="$CMAKE_OPTS -DIPKG_MAINTAINER=$MAINTAINER"
	fi

	cmake -G "$GEN_TYPE" $CMAKE_OPTS $SRC
done

