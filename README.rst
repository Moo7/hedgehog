DESCRIPTION
-----------
Contains an example build environment for embedded systems (e.g. openwrt) using cmake and ipkg.

The cmake directory contains the actual build environment example. The gen.sh script within this directory can be used to generate "Eclipse CDT" and "Unix Makefile" for the following targets:

	- host, native gcc
	- mips, cross-compile using openwrt toolchain

The ipkg directory contains an install script for the ipkg-utils used by the 
build environment example.


